# Løsning til oppgave 3a)
def findEL(num_lst):
    electric_cars = ["EL","EK","EV"]
    res = []
    for car in num_lst:
        if car[:2] in electric_cars:
            res.append(car)
    return res


# Løsning til oppgave 3b)
def getPassedCars(num_lst, year, month, day, hour=-1):
    res = []
    for car in num_lst:
        if car[0] == year and car[1] == month and car[2] == day:
            if hour == -1:
                res.append(car[5])
            else:
                if car[3] == hour:
                    res.append(car[5])
    return res