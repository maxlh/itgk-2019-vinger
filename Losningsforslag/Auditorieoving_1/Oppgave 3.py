# Oppgave 3a)

def oppgave_3a():
    price = int(input("Pris på varene: "))
    payed = int(input("Betalt beløp: "))
    change = payed - price
    if change < 0:
        print("Prisen er", price, "kr. Du betalte bare", payed, "kr. Beløp som mangler er", -change, "kr.")
    else:
        print("Prisen er", price, "kr. Du betalte", payed, "kr. Tilbake", change, "kr.")


# Oppgave 3b)

def oppgave_3b():
    price = int(input("Pris på varene: "))
    payed = int(input("Betalt beløp: "))
    change = payed - price
    while change < 0:
        print("Prisen er", price, "kr. Du har bare betalt", payed, "kr. Beløp som mangler er", -change, "kr.")
        payed += int(input("Hvor mye mer gir du? "))
        change = payed-price
    print("Prisen er", price, "kr. Du betalte", payed, "kr. Tilbake", change, "kr.")


# Oppgave 3c)
# Her finnes det mange mulige løsninger, og vi vil vise to av dem.

# Mulighet 1:
def oppgave_3c_1():
    price = int(input("Pris på varene: "))
    payed = int(input("Betalt beløp: "))
    change = payed - price
    print("Prisen er", str(price) + "kr. Du betalte", str(payed) + "kr. Tilbake:", str(change) + "kr.")

    bills_100 = 0
    bills_50 = 0
    coins_20 = 0
    coins_10 = 0
    coins_5 = 0
    coins_1 = 0

    while change >= 100:
        change -= 100
        bills_100 += 1
    while change >= 50:
        change -= 50
        bills_50 += 1
    while change >= 20:
        change -= 20
        coins_20 += 1
    while change >= 10:
        change -= 10
        coins_10 += 1
    while change >= 5:
        change -= 5
        coins_5 += 1
    while change >= 1:
        change -= 1
        coins_1 += 1

    print("Her er vekslepengene dine:", bills_100, "hundrelapp(er),", bills_50, "femtilapp(er),", coins_20,
          "tjuekrone(r),", coins_10, "tikrone(r),", coins_5, "femkrone(r),", coins_1, "enkrone(r).")


# Mulighet 2:
# Du la kanskje merke til at nesten lik kode ble brukt mange ganger i mulighet 1?
# Dette er veldig tungvint å skrive, og det må jo finnes en bedre måte en copy-paste...
# Som du sikkert har gjettet er det det også!
# I denne mulige løsningen blir funksjoner tatt i bruk, og gjør vår oppgave som programmerere mye lettere :D

def how_many_n_in_num(n, num):
    result = 0
    new_change = num
    while new_change >= n:
        new_change -= n
        result += 1
    return result, new_change


def oppgave_3c_2():
    price = int(input("Pris på varene: "))
    payed = int(input("Betalt beløp: "))
    change = payed-price
    print("Prisen er", str(price) + "kr. Du betalte", str(payed) + "kr. Tilbake:", str(change) + "kr.")

    bills_100, change = how_many_n_in_num(100, change)
    bills_50, change = how_many_n_in_num(50, change)
    coins_20, change = how_many_n_in_num(20, change)
    coins_10, change = how_many_n_in_num(10, change)
    coins_5, change = how_many_n_in_num(5, change)
    coins_1, change = how_many_n_in_num(1, change)

    print("Her er vekslepengene dine:", bills_100, "hundrelapp(er),", bills_50, "femtilapp(er),", coins_20,
          "tjuekrone(r),", coins_10, "tikrone(r),", coins_5, "femkrone(r),", coins_1, "enkrone(r).")


oppgave_3a()
oppgave_3b()
oppgave_3c_1()
oppgave_3c_2()
